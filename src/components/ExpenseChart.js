import React from "react";
// import ReactDOM from "react-dom";
import { Bar } from "react-chartjs-2";


export default class ExpenseChart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        labels: [
          "Jan",
          "Feb",
          "Mar",
          "Apr",
          "May",  
          "Jun",
          "Jul",
          "Aug",
          "Sep",
          "Oct",
          "Nov",
          "Dec"
        ],
        datasets: [
          {
            label: "Monthly Expense",
            backgroundColor: "rgba(192, 57, 43, 0.4)",
            borderColor: "rgba(46, 204, 113, 0.4)",
            borderWidth: 1,
            //stack: 1,
            hoverBorderColor: "rgba(46, 204, 113, 0.6)",
            hoverBackgroundColor: "rgba(192, 57, 43, 0.6)",
            data: [0,0,0,0,0,90800,0,0,0,0,0,0]
          }
        ]
      }
    };
  }

  render() {
    const options = {
      responsive: true,
      legend: {
        display: false
      },
      type: "bar"
      //   scales: {
      //     xAxes: [{
      //         stacked: true
      //     }],
      //     yAxes: [{
      //         stacked: true
      //     }]
      // }
    };
    return (
      <Bar
        data={this.state.data}
        width={null}
        height={null}
        options={options}
      />
    );
  }
}