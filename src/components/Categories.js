import React,{useState,useEffect} from 'react'
import {Card,Button,Modal} from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPen,faTrashAlt } from '@fortawesome/free-solid-svg-icons'
import Swal from 'sweetalert2'
// import incCategories from './AddTransaction'
// import expCategories from './AddTransaction'


export default function Categories(props){
	// console.log(props)

	const edit = <FontAwesomeIcon icon={faPen} />
	const del = <FontAwesomeIcon icon={faTrashAlt} />

	const [incomeCategories, setIncomeCategories] = useState([])
	const [expenseCategories, setExpenseCategories] = useState([])

	const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);
	// const [isActive, setIsActive] = useState(true)

	const [categoryName, setCategoryName] = useState('')
 	const [categoryType, setCategoryType] = useState('')
 	const [categoryId,setCategoryId] = useState('')
 	// const [name,setname] = useState('')
 	const [type,setType] = useState('income')


 	const [showAdd, setShowAdd] = useState(false);
	const handleCloseAdd = () => setShowAdd(false);
	const handleShowAdd = () => setShowAdd(true);
	const [isActive, setIsActive] = useState(true)

	const [addCategoryName, setAddCategoryName] = useState('')
  	const [addCategoryType, setAddCategoryType] = useState('')

 	// console.log(incCategories);
 	// console.log(expCategories);

 	useEffect(()=>{
		if(addCategoryName!==''&&addCategoryType!==''){
	  		setIsActive(true)
		} else {
	  		setIsActive(false)
		}
	},[addCategoryName,addCategoryType])

	function addCategory() {
	    fetch('https://polar-escarpment-62849.herokuapp.com/api/categories/',{
	        method: 'POST',
	        headers: {
	          'Content-Type': 'application/json',
	          'Authorization': `Bearer ${localStorage.getItem('token')}`
	        },
	        body: JSON.stringify({
	          name: addCategoryName,
	          type: addCategoryType
	        })
	    })
	    .then(res=> res.json())
	    .then(data=>{
	        console.log(data);
	        if(data.error){
	          Swal.fire({
	            icon: "error",
	            title: "Failed!",
	            text: data.error
	          })
	        } else {
	          Swal.fire({
	            icon: "success",
	            title: "Success!",
	            text: data.message
	          })
	        setAddCategoryName('')
	        setAddCategoryType('')

	        }
	    })
    
    }

	function submitAndClose(){
		addCategory()
		handleCloseAdd()
	}

 	

	useEffect(()=>{

		fetch('https://polar-escarpment-62849.herokuapp.com/api/categories/',{
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res=>res.json())
		.then(data=>{
			// console.log(data);
			// setAllCategories(data)
			setExpenseCategories(data.filter(category=>category.type==='expense')
				.map(category=>{
					return(
						<Card key={category._id} className="w-50 mx-auto text-center card">
						  <span id={category._id}></span>
						  <Card.Header>{category.name}</Card.Header>
						  <Card.Text>
						      {category.type}
						   </Card.Text>
						  <Card.Footer className="text-muted">
						  	<Button variant="outline-info category-btn" id={category._id} onClick={showAndGetCategory}>{edit}</Button>
						  	<Button variant="outline-danger category-btn" id={category._id} onClick={deleteCategory}>{del}</Button>
						  </Card.Footer>
						</Card>
					)
				}))
			setIncomeCategories(data.filter(category=>category.type==='income')
				.map(category=>{
					return(
						<Card key={category._id} className="w-50 mx-auto text-center card">
						  <span id={category._id}></span>
						  <Card.Header>{category.name}</Card.Header>
						  <Card.Text>
						      {category.type}
						   </Card.Text>
						  <Card.Footer className="text-muted">
						  	<Button variant="outline-info category-btn" id={category._id} onClick={showAndGetCategory}>{edit}</Button>
						  	<Button variant="outline-danger category-btn" id={category._id} onClick={deleteCategory}>{del}</Button>
						  </Card.Footer>
						</Card>
					)
				}))
			
		})
		// .then(window.location.reload())
		
	//has a bug here-infi loop
	},[])
	// },[handleClose,deleteCategory])

	function deleteCategory(e){
		// console.log(e.target.id);
 	// 	// console.log(incomeCategories);
 	// 	incomeCategories.filter(category=>category.id===e.target.id)
 		fetch(`https://polar-escarpment-62849.herokuapp.com/api/categories/${e.target.id}`,{
 			method: 'DELETE',
 			headers: {
 				Authorization: `Bearer ${localStorage.getItem('token')}`
 			}
 		})
 		.then(res=>res.json())
 		.then(data=>{
 			console.log(data);
 		})

 	}

 	function updateCategory(){
		fetch(`https://polar-escarpment-62849.herokuapp.com/api/categories/${categoryId}`,{
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: categoryName,
				type: categoryType
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
		})
 	}


 	function showAndGetCategory(e){
 		// handleShow()
 		// console.log("cliked");
 		fetch(`https://polar-escarpment-62849.herokuapp.com/api/categories/${e.target.id}`,{
 			headers: {
 				Authorization: `Bearer ${localStorage.getItem('token')}`
 			}
 		})
 		.then(res=>res.json())
 		.then(data=>{
 			console.log(data);
 			setCategoryId(data._id)
 			setCategoryName(data.name)
 			setCategoryType(data.type)
 		})
 		.then(handleShow())
 	}

 	function closeandUpdate(){
 		updateCategory()
 		handleClose()
 	}


	
	

	return (
		<>
			<div className="sectionContainer">
				<h2 className="section">Categories</h2>
				<a className="addCat-btn" onClick={handleShowAdd}>Add Category</a>
				<br/>
				<select className="categoryType w-50 selectCategory" value={type} onChange={e => setType(e.target.value)} name="categoryType">
					<option value="income">Income</option>
	                <option value="expense">Expense</option>
	            </select>
	            <div className="entriesOuter">
		        	<div className="entriesInner">
		        		{
		        			type==="income"
		        			?
		        			[incomeCategories].reverse()
		        			:
		        			[expenseCategories].reverse()
		        		}
		        		{/*{incomeComponents}*/}
		        	</div>
		        </div>

			</div>


			


			<Modal show={show} onHide={handleClose}>
	          <Modal.Header closeButton>
	            <Modal.Title>Update Category</Modal.Title>
	          </Modal.Header>
	          <Modal.Body>
	            <label htmlFor="categoryName">Name: </label><br/>
	            <input placeholder="Example. Water Bill" className="w-100" type="text" value={categoryName} onChange={e => {setCategoryName(e.target.value)}}/>
	            <br/>
	            <br/>

	            <label htmlFor="categoryType">Type:</label><br/>
	            <select className="categoryType w-100" name="categoryType" value={categoryType} onChange={e => setCategoryType(e.target.value)}>
	              <option value="" disabled selected>Choose Category Type</option>
	                <option value="income">Income</option>
	                <option value="expense">Expense</option>
	            </select>
	            <br/>
	            <br/>
	            <Button variant="primary" onClick={closeandUpdate}>
		              Update Category
		        </Button>
	          </Modal.Body>
	          <Modal.Footer>
	            <Button variant="secondary" onClick={handleClose}>
	              Close
	            </Button>
	            
	          </Modal.Footer>
	        </Modal>

			<Modal show={showAdd} onHide={handleCloseAdd}>
	          <Modal.Header closeButton>
	            <Modal.Title>Add new Category</Modal.Title>
	          </Modal.Header>
	          <Modal.Body>
	            <label htmlFor="categoryName">Name: </label><br/>
	            <input placeholder="Example. Water Bill" className="w-100" type="text" value={addCategoryName} onChange={e => {setAddCategoryName(e.target.value)}}/>
	            <br/>
	            <br/>

	            <label htmlFor="categoryType">Type:</label><br/>
	            <select className="categoryType w-100" name="categoryType" value={addCategoryType} onChange={e => setAddCategoryType(e.target.value)}>
	              <option value="" disabled selected>Choose Category Type</option>
	                <option value="income">Income</option>
	                <option value="expense">Expense</option>
	            </select>
	            <br/>
	            <br/>
	            {
	            isActive
	            ?
	            <Button variant="primary" onClick={submitAndClose}>
	              Add Category
	            </Button>
	            :
	            <Button variant="primary" disabled>
	              Add Category
	            </Button>
	          }
	          </Modal.Body>
	          <Modal.Footer>
	            <Button variant="secondary" onClick={handleCloseAdd}>
	              Close
	            </Button>
	            
	          </Modal.Footer>
	        </Modal>
		</>





		
	)
}