import React, {useContext} from 'react'
import {GlobalContext} from '../context/GlobalState'
import {numberWithCommas} from '../utils/format'

export default function Balance() {
	const {transactions} = useContext(GlobalContext)

	const amounts = transactions.map(transaction => (transaction.amount));
	// console.log(amounts)

	const total = amounts.reduce((acc, item) => (acc += item), 0).toFixed(2);
	// console.log(total)
	return (
		<>
			<h4>Your Balance</h4>
			<h2>&#8369; {numberWithCommas(total)}</h2>
		</>
	);
}
