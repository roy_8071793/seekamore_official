import React from "react";
// import ReactDOM from "react-dom";
import { Line } from "react-chartjs-2";

export default class MonthlyTrend extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        labels: [
          "Jan",
          "Feb",
          "Mar",
          "Apr",
          "May",  
          "Jun",
          "Jul",
          "Aug",
          "Sep",
          "Oct",
          "Nov",
          "Dec"
        ],
        datasets: [
          {
            label: "Income",
            backgroundColor: "rgba(46, 204, 113, 0.4)",
            borderColor: "rgba(46, 204, 113, 0.4)",
            borderWidth: 2,
            //stack: 1,
            hoverBackgroundColor: "rgba(46, 204, 113, 0.6)",
            hoverBorderColor: "rgba(46, 204, 113, 0.6)",
            data: [0,0,0,0,0,290000,0,0,0,0,0,0]
          },
          {
            label: "Expense",
            backgroundColor: "rgba(192, 57, 43, 0.4)",
            borderColor: "rgba(192, 57, 43, 0.4)",
            borderWidth: 2,
            //stack: 1,
            hoverBackgroundColor: "rgba(192, 57, 43, 0.6)",
            hoverBorderColor: "rgba(192, 57, 43, 0.6)",
            data: [0,0,0,0,0,90800,0,0,0,0,0,0]
          }

          
        ]
      }
    };
  }

  render() {
    const options = {
      responsive: true,
      legend: {
        display: false
      },
      type: "Line"
      //   scales: {
      //     xAxes: [{
      //         stacked: true
      //     }],
      //     yAxes: [{
      //         stacked: true
      //     }]
      // }
    };
    return (
      <Line
        data={this.state.data}
        width={null}
        height={null}
        options={options}
      />
    );
  }
}
