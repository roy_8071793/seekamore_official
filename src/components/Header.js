import React,{useState,useEffect} from 'react'

export default function Header() {
	const [user,setUser] = useState('')

	
	useEffect(()=>{
		fetch('https://polar-escarpment-62849.herokuapp.com/api/users/',{
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res=> res.json())
		.then(data=>{
			setUser(data.username);
		})
	})
	return (
		<h2 className="greeting">
			Hello there, {user}!
		</h2>
	);
}
