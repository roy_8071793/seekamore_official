import React,{useState,useEffect} from 'react'
import {Card,Button, Modal} from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPen,faTrashAlt } from '@fortawesome/free-solid-svg-icons'
import {numberWithCommas} from '../utils/format'

export default function Entries() {
	const edit = <FontAwesomeIcon icon={faPen} />
	const del = <FontAwesomeIcon icon={faTrashAlt} />

	const [entry,setEntry] = useState('income')
	const [incomeComponents,setIncomeComponents] = useState([])
	const [expenseComponents,setExpenseComponents] = useState([])

	const [entryName, setEntryName] = useState('')
 	const [entryType, setEntryType] = useState('')
 	const [entryAmount, setEntryAmount] = useState('')
 	const [entryId,setEntryId] = useState('')

	const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);


	// console.log(entry);
	// console.log(incomeComponents);
	// console.log(expenseComponents);

	useEffect(()=>{
		fetch('https://polar-escarpment-62849.herokuapp.com/api/entries/',{
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res=>res.json())
		.then(data=>{	
			// console.log(data);
			setExpenseComponents(data.filter(entry=>entry.type==='expense')
				.map(entry=>{
					return(
						<Card key={entry._id} className="w-50 mx-auto text-center card">
						  <span id={entry._id}></span>
						  <Card.Header>{entry.category}</Card.Header>
						  <Card.Text>
						      &#8369;{numberWithCommas(Math.abs(entry.amount))}
						   </Card.Text>
						  <Card.Footer className="text-muted">
						  	<Button variant="outline-info entry-btn" onClick={showAndGetEntry} id={entry._id} >{edit}</Button>
						  	<Button variant="outline-danger entry-btn" onClick={deleteEntry} id={entry._id} >{del}</Button>
						  </Card.Footer>
						</Card>
					)
				}))
			setIncomeComponents(data.filter(entry=>entry.type==='income')
				.map(entry=>{
					return(
						<Card key={entry._id} className="w-50 mx-auto text-center card">
						  <span id={entry._id}></span>
						  <Card.Header>{entry.category}</Card.Header>
						  <Card.Text>
						      &#8369;{numberWithCommas(entry.amount)}
						   </Card.Text>
						  <Card.Footer className="text-muted">
						  	<Button variant="outline-info entry-btn" onClick={showAndGetEntry} id={entry._id} >{edit}</Button>

						  	<Button variant="outline-danger entry-btn" onClick={deleteEntry} id={entry._id} >{del}</Button>
						  </Card.Footer>
						</Card>
					)
				}))
		})
	},[])
	// },[deleteEntry,updateEntry,showAndGetEntry])

	function deleteEntry(e){
		fetch(`https://polar-escarpment-62849.herokuapp.com/api/entries/${e.target.id}`,{
 			method: 'DELETE',
 			headers: {
 				Authorization: `Bearer ${localStorage.getItem('token')}`
 			}
 		})
 		.then(res=>res.json())
 		.then(data=>{
 			console.log(data);
 		})
	}

	function updateEntry(){
		fetch(`https://polar-escarpment-62849.herokuapp.com/api/entries/${entryId}`,{
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				category: entryName,
				amount: entryAmount,
				type: entryType
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
		})	
	}

	function showAndGetEntry(e){
 		// handleShow()
 		// console.log("cliked");
 		fetch(`https://polar-escarpment-62849.herokuapp.com/api/entries/${e.target.id}`,{
 			headers: {
 				Authorization: `Bearer ${localStorage.getItem('token')}`
 			}
 		})
 		.then(res=>res.json())
 		.then(data=>{
 			// console.log(data);
 			setEntryId(data._id)
 			setEntryName(data.category)
 			setEntryType(data.type)
 			setEntryAmount(Math.abs(data.amount))
 		})
 		.then(handleShow())
 	}

	function closeandUpdate(){
 		updateEntry()
 		handleClose()
 	}

	return(
		<>
			<div className="sectionContainer entriesSection">
				<h2>Entries</h2>
				<select className="categoryType w-50 selectCategory" value={entry} onChange={e => setEntry(e.target.value)} name="categoryType">
		                <option value="income">Income</option>
		                <option value="expense">Expense</option>
		            </select>
		        <div className="entriesOuter">
		        	<div className="entriesInner">
		        		{
		        			entry==="income"
		        			?
		        			[incomeComponents]
		        			:
		        			[expenseComponents]
		        		}
		        		{/*{incomeComponents}*/}
		        	</div>
		        </div>
			</div>
			<Modal show={show} onHide={handleClose}>
	          <Modal.Header closeButton>
	            <Modal.Title>Update Entry</Modal.Title>
	          </Modal.Header>
	          <Modal.Body>
	            <label htmlFor="categoryName">Name: </label><br/>
	            <input placeholder="Example. Water Bill" className="w-100" type="text" value={entryName} onChange={e => {setEntryName(e.target.value)}}/>
	            <br/>
	            <br/>

	            <label htmlFor="categoryType">Type:</label><br/>
	            <select className="categoryType w-100" name="categoryType" value={entryType} onChange={e => setEntryType(e.target.value)}>
	              <option value="" disabled selected>Choose Category Type</option>
	                <option value="income">Income</option>
	                <option value="expense">Expense</option>
	            </select>
	            <br/>
	            <br/>
	            <label htmlFor="categoryName">Amount: </label><br/>
	            <input placeholder="Enter Amount" className="w-100 entryAmount" type="number" value={entryAmount} onChange={e => {setEntryAmount(e.target.value)}}/>
	            <br/>
	            <br/>
	            <Button variant="primary" onClick={closeandUpdate}>
		              Update Entry
		        </Button>
	          </Modal.Body>
	          <Modal.Footer>
	            <Button variant="secondary" onClick={handleClose}>
	              Close
	            </Button>
	            
	          </Modal.Footer>
	        </Modal>
	    </>
	)
}