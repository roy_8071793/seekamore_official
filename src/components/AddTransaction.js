import React, {useState, useContext, useEffect} from 'react'
import {GlobalContext} from '../context/GlobalState'
// import addCategory from './Nav'

export default function AddTransaction() {
	const [text, setText] = useState('')
	const [amount, setAmount] = useState('')
	const [type,setType] = useState('')	
	const [sign,setSign] = useState('')
	// const [allCategories,setAllCategories] = useState([])
	const [incCategories, setIncCategories] = useState([])
	const [expCategories, setExpCategories] = useState([])
	const {addTransaction} = useContext(GlobalContext)

	const [isActive, setIsActive] = useState(true)

	useEffect(()=>{
		if(text!==''&&amount!==''){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	},[text,amount])

	useEffect(()=>{
		fetch('https://polar-escarpment-62849.herokuapp.com/api/categories/',{
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res=>res.json())
		.then(data=>{
			// console.log(data);
			// setAllCategories(data)
			setExpCategories(data.filter(category=>category.type==='expense')
				.map(category=>{
					return(
						<option key={category._id} value={category.name}>{category.name}</option>
					)
				}))
			setIncCategories(data.filter(category=>category.type==='income')
				.map(category=>{
					return(
						<option key={category._id} value={category.name}>{category.name}</option>
					)
				}))
		})
	//has a bug here-infi loop
	},[])
	// },[addCategory])

	// useEffect(()=>{
	// 	console.log(text);
	// },[text])

	// console.log(expCategories);
	function plus(e){
		setText(e.target.value)
		setSign('+')
		setType('income')
	}

	function minus(e){
		setText(e.target.value)
		setSign('-')
		setType('expense')


	}


	const onSubmit = e => {
		e.preventDefault()

		//fetch here?
		fetch('https://polar-escarpment-62849.herokuapp.com/api/entries/',{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				category: text,
				amount: +`${sign}${amount}`,
				type: type
			})

		})
		.then(res=> res.json())
		.then(data=>{
			console.log(data);
		})

		const newTransaction = {
			id: Math.floor(Math.random()*1000000000),
			category: text,
			amount: +`${sign}${amount}`
		}

		addTransaction(newTransaction)
		setText('')
		setAmount('')

	}
	// console.log(text);
	return (
		<>
			<h3>Add new transaction</h3>
			<form onSubmit={onSubmit}>
				<div className="categoryContainer">
					{/*<select className="income selectCategory" value={text} name="income">*/}
					<select className="income selectCategory" value={text} onChange={e => plus(e)} name="income">
						<option value="" disabled selected>Income Category</option>
					    {incCategories}
				 	</select>
				 	<select className="expense selectCategory" value={text} onChange={e => minus(e)} name="expense">
						<option value="" disabled selected>Expense Category</option>
					    {expCategories}
				 	</select>
				</div>
				<br/>
				{/*<div className="form-control addTransaction">*/}
					{/*<label htmlFor="text">Text</label>*/}
					{/*<input type="text" value={text} onChange={e=>setText(e.target.value)} placeholder="Enter text..." />*/}
				{/*</div>*/}
				<br/>
				<div className="form-control addTransaction">
				{/*	<label htmlFor="amount">
						Amount
						<br />
						(negative - expense, positive - income)</label>*/}
					<input type="number" value={amount} onChange={e=>setAmount(e.target.value)} placeholder="Enter amount..." />
				</div>
				<br/>
				{
					isActive
					?
					<button className="btn">Add transaction</button>
					:
					<button className="btn" disabled>Add transaction</button>

				}
				
			</form>
		</>
	);
}
