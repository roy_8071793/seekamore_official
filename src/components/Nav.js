import React, {useContext, useState, useEffect} from 'react'
import {Navbar,Nav, Modal, Button} from 'react-bootstrap'
import {Link,NavLink} from 'react-router-dom'
import UserContext from '../userContext'
import logo from '../assets/sycamore.png'
import Swal from 'sweetalert2'

export default function NavBar() {

  const {user} = useContext(UserContext)

  return (
      <>
        <Navbar variant="dark" expand="lg" className="navbar">
          <Navbar.Brand className="brand" as={Link} to="/">
            <img className="logo " src={logo} alt=""/>
             <h3>Seekamore</h3></Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav" className="nav-links">
            <Nav className="ml-auto">
              {
                user.email
                ?
                <div className="ml-auto navitems">
                <Nav.Link as={NavLink} to="/tracker">My Tracker</Nav.Link>
                <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
                </div>
                :
                <div className="ml-auto navitems">
                  <Nav.Link as={NavLink} to="/signup">Signup</Nav.Link>
                  <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                </div>
              }
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </>

  );
}