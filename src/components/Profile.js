import React,{useEffect} from 'react'

export default function Profile(){
	useEffect(()=>{
		fetch('https://polar-escarpment-62849.herokuapp.com/api/users',{
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data)
		})
	},[])

	useEffect(()=>{
		fetch('https://polar-escarpment-62849.herokuapp.com/api/images/uploadimage',{
			method:'POST',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data)
		})
	})
	

	return(
		<>
			<div>This is profile</div>
			<div>
                <label for="image">Upload Image</label>
                <input type="file" id="image"
                       name="image" value="" required/>
            </div>
		</>
	)
}