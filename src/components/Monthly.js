import React,{useState} from 'react'
import IncomeChart from './IncomeChart'
import ExpenseChart from './ExpenseChart'


export default function Monthly() {
	// console.log(IncomeChart);
	const [type,setType] = useState('income')
	// console.log(type);

	return(
		<div className="sectionContainer">
			<h2>Monthly</h2>
			<select className="categoryType w-50 selectCategory" value={type} onChange={e => setType(e.target.value)} name="categoryType">
	              <option value="" disabled selected>Choose Category Type</option>
	                <option value="income">Income</option>
	                <option value="expense">Expense</option>
	            </select>
	        <div className="entriesOuter">
	        	<div className="entriesInner">
	        		{
	        			type==='income'
	        			?
	        			<IncomeChart/>
	        			:
	        			<ExpenseChart/>
	        		}
	        	</div>
	        </div>
		</div>
	)
}