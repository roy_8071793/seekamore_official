import React, {useContext, useEffect} from 'react'
import {Transaction} from './Transaction'
import {GlobalContext} from '../context/GlobalState'


export default function TransactionList() {

	const {transactions, getTransactions, addTransaction} = useContext(GlobalContext)
	// console.log(context);
	useEffect(()=>{
		getTransactions()
	},[])
	// },[getTransactions])

	return (
		<>
			<h3>History</h3>
			<div className="transactionsInnerContainer">
				<ul className="list">
					{transactions.map(transaction=>(<Transaction key={transaction._id} transaction={transaction} />))}
				</ul>
			</div>
		</>
	);
}
