import React, {useContext} from 'react'
import {GlobalContext} from '../context/GlobalState'
import {numberWithCommas} from '../utils/format'

export const Transaction = ({transaction}) => {
	// const {transactions} = useContext(GlobalContext)
	const {deleteTransaction} = useContext(GlobalContext)
	const sign = transaction.amount < 0 ? '-' : '+'
	// console.log(context);
	return (
		<li key={transaction._id} className={transaction.amount < 0 ? 'minus' : 'plus'}>
			{transaction.category}
			<span>{sign} &#8369;{numberWithCommas(Math.abs(transaction.amount))}</span>
			<button onClick={()=>deleteTransaction(transaction._id)} className="delete-btn">X</button>
		</li>
	);
}
