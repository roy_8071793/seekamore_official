import React,{useState, useEffect, useContext} from 'react'
import {Form, Button, Container} from 'react-bootstrap'

import Swal from 'sweetalert2'

import {Redirect} from 'react-router-dom'

import UserContext from '../userContext'

export default function Login(){

	const {user,setUser} = useContext(UserContext)
	// console.log(user);

	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')

	const [isActive, setIsActive] = useState(true)

	const [willRedirect, setWillRedirect] = useState(false)

	useEffect(()=>{

		if(email!==''&&password!==''){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	},[email,password])

	function loginUser(e){
		e.preventDefault()
		

		fetch('https://polar-escarpment-62849.herokuapp.com/api/users/login',{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				username: email,
				email: email,
				password: password
			})
		})
		.then(res=>res.json())
		.then(data=>{
			// console.log(data)
			if(data.accessToken){
				localStorage.setItem('token',data.accessToken)
				Swal.fire({
					icon: "success",
					title: "Logged In Successfully.",
					text: "Thank you for logging in."
				})
				fetch('https://polar-escarpment-62849.herokuapp.com/api/users/',{
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res=>res.json())
				.then(data=>{
					// console.log(data)
					localStorage.setItem('email',data.email)
					localStorage.setItem('isAdmin',data.isAdmin)

					setWillRedirect(true)

					setUser({
						email: data.email,
						isAdmin: data.isAdmin
					})
				})
				setEmail('')
				setPassword('')
			} else {
				Swal.fire({
					icon: "error",
					title: "Login Failed",
					text: data.err
				})
			}
			// console.log(user);

			
		})

		

	}
	
	return(
		willRedirect
		?
		<Redirect to="/tracker"/>
		:
		<Container className="formContainer w-50">
			<h3>Login</h3>
			<Form onSubmit={e=> loginUser(e)}>
				<Form.Group>
					{/*<Form.Label>Email:</Form.Label>*/}
					<Form.Control className="formInput" type="test" placeholder="Username or Email" value={email} onChange={e => {setEmail(e.target.value)}} required/>
				</Form.Group>

				<Form.Group>
					{/*<Form.Label>Password:</Form.Label>*/}
					<Form.Control className="formInput" type="password" placeholder="Password" value={password} onChange={e => {setPassword(e.target.value)}} required/>
				</Form.Group>

				{
					isActive
					?
					<Button variant="dark" type="submit">Sign In</Button>
					:
					<Button variant="dark" type="submit" disabled>Sign In</Button>
				}
				
			</Form>
		</Container>
	)
}
