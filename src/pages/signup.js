import React, {useState, useEffect} from 'react'
import {Form, Button, Container} from 'react-bootstrap'
import Swal from 'sweetalert2'
import {Redirect} from 'react-router-dom'

export default function Signup(){

	const [username, setUsername] = useState('')
	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [mobileNo, setMobileNo] = useState('')
	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [confirmPassword, setConfirmPassword] = useState('')
	const [willRedirect, setWillRedirect] = useState(false)

	const [isActive, setIsActive] = useState(true)

	useEffect(()=>{
		if((username!==''&& firstName!==''&& lastName!==''&& mobileNo!==''&& email!==''&& password!==''&& confirmPassword!=='') && (mobileNo.length===11) && (password===confirmPassword)){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	},[username,firstName,lastName,email,mobileNo,password,confirmPassword])
	

	function registerUser(e){
		e.preventDefault()

		fetch('https://polar-escarpment-62849.herokuapp.com/api/users/',{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				username: username,
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				password: password,
				confirmPassword: confirmPassword
			})
		})
		.then(res => res.json())
		.then(data=>{
			console.log(data)
			Swal.fire({
				icon: "success",
				title: "Registration Success.",
				text: "Thank you for registering."
			})
			setUsername('')
			setFirstName('')
			setLastName('')
			setMobileNo('')
			setEmail('')
			setPassword('')
			setConfirmPassword('')
			setWillRedirect(true)
			
		})

	}

	return(
		willRedirect
		?
		<Redirect to="/login"/>
		:
		<Container className="formContainer w-50">
			<h3>Sign up</h3>
			<Form  onSubmit={e=> registerUser(e)}>
				<Form.Group>
				    {/*<Form.Label>Username:</Form.Label>*/}
				    <Form.Control id="test" className="formInput" type="text" placeholder="Username" value={username} onChange={e => {setUsername(e.target.value)}} required/>
				</Form.Group>

				<Form.Group >
				    {/*<Form.Label>First Name:</Form.Label>*/}
				    <Form.Control className="formInput" type="text" placeholder="First name" value={firstName} onChange={e => {setFirstName(e.target.value)}} required/>
				</Form.Group>

				<Form.Group>
					{/*<Form.Label>Last Name:</Form.Label>*/}
					<Form.Control className="formInput" type="text" placeholder="Last name" value={lastName} onChange={e => {setLastName(e.target.value)}} required/>
				</Form.Group>

				<Form.Group>
					{/*<Form.Label>Moile No:</Form.Label>*/}
					<Form.Control className="formInput numberInput" type="number" placeholder="11-digit mobile number" value={mobileNo} onChange={e => {setMobileNo(e.target.value)}} required/>
				</Form.Group>

				<Form.Group>
					{/*<Form.Label>Email:</Form.Label>*/}
					<Form.Control className="formInput" type="email" placeholder="Email" value={email} onChange={e => {setEmail(e.target.value)}} required/>
					{/*<Form.Text className="text-muted">
					  We'll never share your email with anyone else.
					</Form.Text>*/}
				</Form.Group>

				<Form.Group>
					{/*<Form.Label>Password:</Form.Label>*/}
					<Form.Control className="formInput" type="password" placeholder="Password" value={password} onChange={e => {setPassword(e.target.value)}} required/>
				</Form.Group>

				<Form.Group>
					{/*<Form.Label>Confirm Password:</Form.Label>*/}
					<Form.Control className="formInput" type="password" placeholder="Confirm password" value={confirmPassword} onChange={e => {setConfirmPassword(e.target.value)}}  required/>
				</Form.Group>
				{
					isActive
					?
					<Button variant="dark" type="submit">Sign Up</Button>
					:
					<Button variant="dark" type="submit" disabled>Sign Up</Button>
				}
				
			</Form>
		</Container>
	)
}



