import React from 'react'
import Header from '../components/Header'
import Balance from '../components/Balance'
import IncomeExpense from '../components/IncomeExpense'
import TransactionList from '../components/TransactionList'
import AddTransaction from '../components/AddTransaction'
import Categories from '../components/Categories'
import Entries from '../components/Entries'
import Monthly from '../components/Monthly'
import Trend from '../components/Trend'
// import Profile from '../components/Profile'
import {Row,Col} from 'react-bootstrap'

import {GlobalProvider} from '../context/GlobalState'

export default function Tracker(){
	// console.log(this);

	function reRender(){
		console.log("parent re-rendered");
		// this.forceUpdate()
	}
	console.log("parent re-rendered");

	return(
		<Row className="mainContainer">
			<Col>
				<Categories render={reRender} />
				<Entries />
			</Col>
			<Col className="trackerContainer">
				<GlobalProvider>
			      <Header />
			      <div className="trackerInnerContainer">
			        <Balance />
			        <IncomeExpense />
			        <div className="transactionsOuterContainer">
			        		<TransactionList />
			        </div>
			        <AddTransaction  />
			      </div>
			    </GlobalProvider>
	   		</Col>
	   		<Col>
	   			<Monthly />
				<Trend />
	   		</Col>
	    </Row>

	)
}