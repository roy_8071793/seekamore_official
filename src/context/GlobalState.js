import React, {createContext, useReducer} from 'react'
import AppReducer from './AppReducer'

const initialState = {
	transactions: [],
	loading: true
}

export const GlobalContext = createContext(initialState)

export const GlobalProvider = ({children})=>{
	const [state, dispatch] = useReducer(AppReducer, initialState)

	async function getTransactions(){
		fetch('https://polar-escarpment-62849.herokuapp.com/api/entries/',{
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res=> res.json())
		.then(data=>{
			// console.log(data)
			dispatch({
				type: 'GET_TRANSACTIONS',
				payload: data.reverse()
			})
		})
	}

	function deleteTransaction(id){
		fetch(`https://polar-escarpment-62849.herokuapp.com/api/entries/${id}`,{
			method: 'DELETE',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res=> res.json())
		.then(data=>{
			console.log(data)
			dispatch({
			type: 'DELETE_TRANSACTION',
			payload: data
		})
		})

		dispatch({
			type: 'DELETE_TRANSACTION',
			payload: id
		})
	}

	function addTransaction(transaction){

		dispatch({
			type: 'ADD_TRANSACTION',
			payload: transaction
		})
	}

	return(<GlobalContext.Provider value={{
		transactions: state.transactions,
		loading: state.loading,
		getTransactions,
		deleteTransaction,
		addTransaction
		}}>
			{children}
		</GlobalContext.Provider>)
}