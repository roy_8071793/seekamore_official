import './App.css';
import {Container} from 'react-bootstrap'

import Nav from './components/Nav'
import Home from './pages/home'
import Login from './pages/login'
import Logout from './pages/logout'
import Signup from './pages/signup'
import Tracker from './pages/tracker'

import {BrowserRouter as Router} from 'react-router-dom'
import {Route,Switch} from 'react-router-dom'
import {useState} from 'react'
import {UserProvider} from './userContext'



function App() {

  const [user,setUser] = useState({
    email: localStorage.getItem('email'),
    isAdmin: localStorage.getItem('isAdmin')==="true"
  })
  const unsetUser = () => {
    localStorage.clear()
  }


  return (
    // <UserProvider value={{user,setUser, unsetUser}}>
    //   <Router>
    //     <Nav />
    //     <Container fluid>
    //       <Switch>
    //         <Route exact path="/" component={Home}/>
    //         <Route exact path="/signup" component={Signup}/>
    //         <Route exact path="/login" component={Login}/>
    //         <Route exact path="/logout" component={Logout}/>
    //         <Route exact path="/tracker" component={Tracker}/>
    //       </Switch>
    //     </Container>
    //   </Router>
    // </UserProvider>
    <div className="maintenance">site is under maintenance</div>

  );
}

export default App;
